<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=MAIN_URL?>/components/images/user1.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <center><p><strong><?= $_SESSION['nama_lengkap']?></strong></p></center>
          <sub><p><i>Admin Master</i></p><sub>
          <a href="#"></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
          
        <li class="header">MAIN NAVIGATION</li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Akun</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= MAIN_URL ?>/pages/FRM_Akun.php"><i class="fa fa-home"></i> Tambah Akun</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/LST_Akun.php"><i class="fa fa-th-list"></i> List Akun</a></li>
            
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-suitcase"></i>
            <span>Data LPJ</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= MAIN_URL ?>/pages/FRM_PenyetoranLPJ.php"><i class="fa fa-shopping-cart"></i> Input LPJ</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/LST_LPJ.php"><i class="fa fa-download"></i> List LPJ</a></li>
            <li><a href="<?= MAIN_URL ?>/pages/LST_penanggungjawab.php"><i class="fa fa-upload"></i> Penanggungjawab</a></li>
                       
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Data Monitoring</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
              <ul class="treeview-menu">
                <li><a href="<?= MAIN_URL ?>/pages/FRM_monev.php"><i class="fa fa-money"></i> Upload Hasil Monitoring</a></li>
                <li><a href="<?= MAIN_URL ?>/pages/LST_monev.php"><i class="fa fa-sitemap"></i> Data Monitoring</a></li>
                
              </ul>
            </li>
            
        
        <li class="treeview" onclick="location.href='<?= MAIN_URL ?>/pages/form_info.php'">
          <a href="#">
                <i class="fa fa-info"></i>
                <span>Info Perusahaan</span>
          </a>
        </li>
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

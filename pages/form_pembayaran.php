<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Pembayaran";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Form Pembayaran";
    $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Pembayaran -->
                <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/pembayaran_event.php">
                    <!-- Nama Event -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Event</label>
                        
                        <div class="col-sm-6">
                            <select class="form-control select2" name="nama_event" id="nama_event">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_booking","id_booking, nama_event, no_bukti_pemesanan,status_booking",NULL,"is_final = '0' AND status_booking NOT IN ('CANCELLED')");
                                    $result_n = $db->getResult();
                                    foreach($result_n as $show_n){
                                ?>
                                <option value="<?=$show_n["id_booking"];?>"><?= $show_n["nama_event"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <span id="data">
                        
                    </span>
                    
                    <div style="margin-left:15px">
                        <h4><u>Informasi Pembayaran</u></h4>
                    </div>
                    
                    <!-- Tanggal Pembayaran -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Pembayaran</label>
                        
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="datepicker form-control" name="tanggal_pembayaran" required/>
                            </div>
                        </div>
                    </div>
                    
                    <span id="bayar">
                        
                    </span>
                    
                    <!-- Cara Pembayaran -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Cara Pembayaran</label>
                        
                        <div class="col-sm-3">
                            <select class="form-control select2" name="cara_pembayaran">
                                <option value=""> ---</option>
                                <option value="TUNAI">Tunai</option>
                                <option value="BANK">Bank</option>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Dibayar ke -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Dibayar ke Akun</label>
                        
                        <div class="col-sm-4">
                            <select class="select2 form-control" name="dibayar_ke">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa,kode_parent",NULL,"kode_parent ='110000' AND jenis_coa = 'DETAIL'");
                                    $result_dk = $db->getResult();
                                    foreach($result_dk as $show_dk){
                                ?>
                                <option value="<?= $show_dk["kode_coa"]; ?>"><?= $show_dk["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
    
    <!-- Clockpicker -->
    <script>
        $(document).ready(function(){
            $(".clockpicker").clockpicker({donetext:"SET"});
        });
    </script>
    
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>
    
    <!-- iCheck for checkbox and radio inputs -->
    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass   : 'iradio_minimal-blue'
        });
    </script>
    
    <!-- Flat red color scheme for iCheck -->
        <script>
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
              checkboxClass: 'icheckbox_flat-green',
              radioClass   : 'iradio_flat-green'
            });
        </script>
    
    <!-- -->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#nama_event").change(function(){
                var nama_event = $("#nama_event").val();
                $.ajax
                ({
                    url: "<?= MAIN_URL ?>/action/act_get_data_pembayaran.php",
                    data: "nama_event="+nama_event,
                    cache: false,
                    success: function(msg)
                    {
                        $("#data").html(msg);
                    }
                });
            });
        });
    </script>
    
    <!-- -->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#nama_event").change(function(){
                var nama_event = $("#nama_event").val();
                $.ajax({
                    url: "<?= MAIN_URL ?>/action/act_get_data_pembayaran2.php",
                    data: "nama_event="+nama_event,
                    cache: false,
                    success: function(msg)
                        {
                            $("#bayar").html(msg);
                        }
                });
            });
        });
    </script>
    
<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_barang","id_barang,kode_barang,nama_barang,jenis_barang,id_satuan_besar,id_satuan_kecil,konversi_satuan,is_inventory,avg_price,on_stock,on_purchased",NULL,"id_barang='$id'");
            $result = $db->getResult();
            foreach($result as $show_dbj){

?>
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_update_data_barangjasa.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <input type="hidden" name="id_barang" id="id_barang" value="<?= $id ?>"/>
                <!-- Kode Barang -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Kode</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="kode_barang" placeholder="Kode Barang" value="<?= $show_dbj["kode_barang"]; ?>">
                    </div>
                </div>
                
                <!-- Nama Barang -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama</label>
                        
                    <div class="col-sm-5">
                       <input type="text" class="form-control" name="nama_barang" placeholder="Nama Barang" value="<?= $show_dbj["nama_barang"]; ?>">
                    </div>
                </div>
                
                <!-- Kode Satuan Besar -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Kode Satuan Besar</label>
                        
                    <div class="col-sm-6">
                        <select class="form-control select2" name="satuan_besar">
                            <?php
                                $idd = $show_dbj["id_satuan_besar"];
                                $db->select("tb_satuan","kode_satuan,nama_satuan",NULL,"id_satuan='$idd'");
                                $hasil = $db->getResult();
                                foreach($hasil as $fh){
                            ?>
                            <option value="<?= $show_dbj["id_satuan_besar"]; ?>"><?= $fh["nama_satuan"]; ?></option>
                                <?php } ?>
                            <option value=""> ---</option>
                            <?php
                                $db->select("tb_satuan","id_satuan,kode_satuan,nama_satuan",NULL,"is_active='1'");
                                $result = $db->getResult();
                                foreach($result as $satuan){
                            ?>
                            <option value="<?= $satuan['id_satuan']; ?>"><?= $satuan['kode_satuan']; ?> - <?= $satuan['nama_satuan']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <!-- Kode Satuan Kecil -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Kode Satuan Kecil</label>
                        
                    <div class="col-sm-6">
                        <select class="form-control select2" name="satuan_kecil">
                            <?php
                                $idx = $show_dbj["id_satuan_kecil"];
                                $db->select("tb_satuan","kode_satuan,nama_satuan",NULL,"id_satuan='$idx'");
                                $hasil = $db->getResult();
                                foreach($hasil as $fx){
                            ?>
                            <option value="<?= $show_dbj["id_satuan_kecil"]; ?>"><?= $fx["nama_satuan"]; ?></option>
                                <?php } ?>
                            <option value=""> ---</option>
                            <?php
                                $db->select("tb_satuan","id_satuan,kode_satuan,nama_satuan",NULL,"is_active='1'");
                                $results = $db->getResult();
                                foreach($results as $satuan_k){
                            ?>
                            <option value="<?= $satuan_k['id_satuan']; ?>"><?= $satuan_k['kode_satuan']; ?> - <?= $satuan_k['nama_satuan']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <!-- Nilai Konversi Satuan -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nilai Konversi Satuan</label>
                        
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="konversi_satuan" value="<?= $show_dbj["konversi_satuan"]; ?>">
                    </div>
                </div>
                
                <!-- Jenis -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Jenis</label>
                        
                    <div class="col-sm-5">
                        <select class="form-control select2" name="jenis">
                            <option value="<?= $show_dbj["jenis_barang"]; ?>"><?= $show_dbj["jenis_barang"]; ?></option>
                            <option value=""> ---</option>
                            <option value="BARANG">Barang</option>
                            <option value="JASA">Jasa</option>
                        </select>
                    </div>
                </div>
                
                <!-- Harga Rata - rata -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Harga Rata - rata</label>
                        
                    <div class="col-sm-5">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="harga_rata2" value="<?= $show_dbj["avg_price"]; ?>">
                            </div>
                        </div>
                </div>
                
                <!-- On Stock -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">On Stock</label>
                        
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="on_stock" value="<?= $show_dbj["on_stock"]; ?>">
                    </div>
                </div>
                
                <!-- On Purchased -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">On Purchased</label>
                        
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="on_purchased" value="<?= $show_dbj["on_purchased"]; ?>">
                    </div>
                </div>
                
                <!-- Inventory -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>

                    <div class="col-sm-4">
                        <input type="checkbox" class="flat-red" id="inventory" name="inventory" value="yes"> Masuk Inventory
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>



<!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>

<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Refund Dana";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Refund Dana";
    $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Refund -->
                <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_simpan_refunddana.php">
                    <div style="margin-left:15px">
                        <h4><u>Informasi Transaksi</u></h4>
                    </div>
                    
                    <!-- Tanggal Transaksi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tanggal_transaksi" placeholder="Tanggal Transaksi">
                        </div>
                    </div>
                    
                    <!-- Jenis Refund -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Refund</label>
                        
                        <div class="col-sm-3">
                            <select class="select2 form-control" name="jenis_refund">
                                <option value=""> ---</option>
                                <option value="310004">Jaminan Kerusakan</option>
                                <option value="510001">Refund Sewa</option>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Nama Event -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Event</label>
                        
                        <div class="col-sm-3">
                            <select class="select2 form-control" name="nama_event">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_booking","id_booking,nama_event",NULL,"is_final='0' and status_booking='TENTATIVE' ");
                                    $result_ne = $db->getResult();
                                    foreach($result_ne as $show_ne){
                                ?>
                                <option value="<?= $show_ne["id_booking"]; ?>"><?= $show_ne["nama_event"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Keterangan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        
                        <div class="col-md-3">
                            <textarea class="form-control" name="keterangan"></textarea>
                        </div>
                    </div>
                    
                    <!-- Jumlah -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah</label>
                        
                        <div class="col-md-3">
                            <input type="number" class="form-control" name="jumlah"/>
                        </div>
                    </div>
                    
                    <!-- Cara Pembayaran -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Cara Pembayaran</label>
                        
                        <div class="col-md-2">
                            <select class="select2 form-control" name="cara_pembayaran">
                                <option value=""></option>
                                <option value="TUNAI">Tunai</option>
                                <option value="BANK">Bank</option>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Akun Kas / Bank -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akun Kas / Bank</label>
                        
                        <div class="col-sm-3">
                            <select class="select2 form-control" name="akun_kas">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_parent='110000' ");
                                    $result_ak = $db->getResult();
                                    foreach($result_ak as $show_ak){
                                ?>
                                    <option value="<?= $show_ak['kode_coa'];?>"><?= $show_ak["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                    
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php
    $template->endContent();
?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
        
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>

<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php
    $template->endHtml();
?>
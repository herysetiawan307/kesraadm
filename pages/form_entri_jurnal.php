<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Entri Jurnal";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Entri Jurnal";
    $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Entri Jurnal -->
                <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_entri_jurnal.php">
                    <div style="margin-left:15px">
                        <h4><u>Informasi Transaksi</u></h4>
                    </div>
                    <!-- Tanggal Transaksi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control datepicker" name="tanggal_transaksi" placeholder="Tanggal Transaksi">
                        </div>
                    </div>
                    
                    <!-- No. Bukti -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No. Bukti</label>
                        
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="no_bukti" placeholder="No. Bukti"/>
                        </div>
                    </div>
                    
                    <!-- Keterangan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="keterangan" placeholder="Keterangan"/>
                        </div>
                    </div>
                    
                    <div style="margin-left:15px">
                        <h4><u>Akun - akun Transaksi</u></h4>
                    </div>
                    
                    <!-- Kode Akun -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akun</label>
                        
                        <div class="col-sm-3">
                            <select class="form-control select2" name="kode_akun">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa,kode_parent,jenis_coa,saldo_normal",NULL,"kode_parent NOT IN(0) ");
                                    $result_k_a = $db->getResult();
                                    foreach($result_k_a as $show_k_a ){
                                ?>
                                <option value="<?= $show_k_a["kode_coa"]; ?>"><?= $show_k_a["kode_coa"]; ?> - <?= $show_k_a["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    
                    <!-- Posisi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Posisi</label>
                        
                        <div class="col-sm-3">
                            <select class="form-control select2" name="posisi">
                                <option value=""> ---</option>
                                <option value="D">Debet</option>
                                <option value="K">Kredit</option>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Nominal Transaksi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nominal Transaksi</label>
                        
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="nominal_transaksi">
                            </div>
                        </div>
                    </div>
                    
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
    
    <!-- Clockpicker -->
    <script>
        $(document).ready(function(){
            $(".clockpicker").clockpicker({donetext:"SET"});
        });
    </script>
    
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>
    
    
<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>
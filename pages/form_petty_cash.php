<!--
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ -->
<?php
    //Required File
    require_once dirname(__FILE__)."/../components/templates/main.php";
    require_once dirname(__FILE__)."/../class/config.php";
    
    //Define Connection -> Database
    $db = new Database();
    $db->connect();
    
    //Call Template
    $template = new Template();
    
    //Start HTML
    $template->pageTitle="BHUMIKU Balai Pertemuan | Petty Cash";
    
    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> Petty Cash";
    $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
                <!-- Form Petty Cash -->
                <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_petty_cash.php">
                    <div style="margin-left:15px">
                        <h4><u>Informasi Transaksi</u></h4>
                    </div>
                    
                    <!-- No.Bukti -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No.Bukti</label>
                        
                        <div class="col-sm-2">
                            <input type="text" name="no_bukti" class="form-control" readonly/>
                        </div>
                    </div>
                    
                    <!-- Tanggal Transaksi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                        
                        <div class="col-sm-2">
                            <input type="text" name="tanggal_transaksi" class="form-control datepicker"/>
                        </div>
                    </div>
                    
                    <!-- Catatan -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Catatan</label>
                        
                        <div class="col-sm-4">
                            <textarea class="form-control" name="catatan"></textarea>
                        </div>
                    </div>
                    
                    <!-- Event -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Event</label>
                        
                        <div class="col-sm-4">
                            <select class="form-control select2" name="nama_event">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_booking","id_booking,nama_event",NULL,"is_final='0'");
                                    $result_na = $db->getResult();
                                    foreach($result_na as $show_na){
                                ?>
                                    <option value="<?= $show_na["id_booking"]; ?>"><?= $show_na["nama_event"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div style="margin-left:15px">
                        <h4><u>Akun Transaksi</u></h4>
                    </div>
                    
                    <!-- Nominal Transaksi -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nominal Transaksi</label>
                        
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon">Rp.</span>
                                <input type="number" class="form-control" name="nominal_transaksi">
                            </div>
                        </div>
                    </div>
                    
                    <!-- Akun Sumber Dana -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Akun Sumber Dana</label>
                        
                        <div class="col-sm-4">
                            <select class="form-control select2" name="akun_sumber">
                                <option value=""> ---</option>
                                <?php
                                    $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_parent in('110000','120000') ");
                                    $result_as = $db->getResult();
                                    foreach($result_as as $show_as){
                                ?>
                                <option value="<?= $show_as["kode_coa"]; ?>"><?= $show_as["kode_coa"]; ?> - <?= $show_as["nama_coa"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <!-- Penerima Dana -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Penerima Dana</label>
                        
                        <div class="col-sm-5">
                            <input type="text" class="form-control"  name="penerima_dana"/>
                        </div>
                    </div>
                    
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        
                        <div class="col-sm-1">
                            <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </form>
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

<!-- Place Script Here -->
    <!-- Datepicker -->
    <script>
        $(document).ready(function(){
            $("input.datepicker").Zebra_DatePicker();
        });
    </script>
    
    <!-- Clockpicker -->
    <script>
        $(document).ready(function(){
            $(".clockpicker").clockpicker({donetext:"SET"});
        });
    </script>
    
    <!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>
<!--// End Script Place -->

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>
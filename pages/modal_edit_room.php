<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //Required File
        require_once dirname(__FILE__)."/../class/config.php";
        require_once dirname(__FILE__)."/../components/templates/main.php";
        
    //Call Template
        $template = new Template();
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_ruangan","id_ruangan,nama_ruangan,kapasitas_ruangan,lebar_ruangan,panjang_ruangan,harga_sewa,harga_half_day,harga_hourly",NULL,"id_ruangan='$id'");
            $result = $db->getResult();
            foreach($result as $show_dr){

?>

            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_update_data_room.php">
                <div style="margin-left:15px">
                    <h4><u>Informasi Umum</u></h4>
                </div>
                <!-- ID Room -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">ID Ruangan</label>
                        
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="id_room" placeholder="ID Room" value="<?= $show_dr["id_ruangan"]; ?>" readonly>
                    </div>
                </div>
                
                <!-- Nama Room -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Ruangan</label>
                        
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="nama_room" placeholder="Nama Room" value="<?= $show_dr["nama_ruangan"]; ?>">
                    </div>
                </div>
                
                <!-- Kapasitas Room --> 
                <div class="form-group">
                    <label class="col-sm-4 control-label">Kapasitas Ruangan</label>
                        
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="kapasitas_room" placeholder="Kapasitas Room" value="<?= $show_dr["kapasitas_ruangan"]; ?>">
                    </div>
                </div>
                
                <div style="margin-left:15px">
                    <h4><u>Dimensi Ruangan</u></h4>
                </div>
                
                <!-- Dimensi Room -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Dimensi Ruangan</label>
                        
                    <div class="col-sm-3">
                        <small><i>Panjang</i></small>
                        <div class="input-group">
                            <input type="number" class="form-control" name="panjang" id="panjang" value="<?= $show_dr["panjang_ruangan"]; ?>">
                            <span class="input-group-addon">m</span>
                        </div>
                    </div>
                    
                     <div class="col-sm-3">
                        <small><i>Lebar</i></small>
                        <div class="input-group">
                            <input type="number" class="form-control" name="lebar" id="lebar" value="<?= $show_dr["lebar_ruangan"]; ?>">
                            <span class="input-group-addon">m</span>
                        </div>
                    </div>
                </div>
                
                <div style="margin-left:15px">
                        <h4><u>Harga Sewa Standar</u></h4>
                </div>
                
                <!-- Full Day -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Full Day</label>
                        
                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="full_day" id="full_day" placeholder="Harga Full Day" value="<?= $show_dr["harga_sewa"]; ?>">
                        </div>
                    </div>
                </div>
                
                <!-- Half Day -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Half Day</label>
                        
                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="half_day" id="half_day" placeholder="Harga Half Day" value="<?= $show_dr["harga_half_day"]; ?>">
                        </div>
                    </div>
                </div>
                
                <!-- Hourly Extra -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Hourly Extra</label>
                        
                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon">Rp.</span>
                            <input type="number" class="form-control" name="hourly" id="hourly" placeholder="Hourly Extra" value="<?= $show_dr["harga_hourly"]; ?>">
                        </div>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="simpan btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="reset btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>

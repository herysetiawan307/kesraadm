<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_vendor","id_vendor,nama_vendor,alamat_vendor,telepon_vendor,hp_vendor,cp_vendor,email_vendor,npwp_vendor,bank_vendor,remark_vendor,no_rekening_vendor",NULL,"id_vendor='$id'");
            $result = $db->getResult();
            foreach($result as $show_dv){

?>

            <form class="form-horizontal" method="POST" action="<?=  MAIN_URL ?>/action/act_update_data_vendor.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Vendor -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">ID Vendor</label>
                        
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="id_vendor" placeholder="ID Vendor" readonly value="<?= $show_dv['id_vendor']; ?>">
                    </div>
                </div>
                
                <!-- Nama Vendor -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Vendor</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama_vendor" placeholder="Nama Vendor" value="<?= $show_dv["nama_vendor"]; ?>" required>
                    </div>
                </div>
                
                <!-- Alamat Vendor -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Alamat Vendor</label>
                        
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="alamat_vendor" placeholder="Alamat Vendor" value="<?= $show_dv["alamat_vendor"]; ?>" required>
                    </div>
                </div>
                
                <!-- Telepon Vendor -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Telepon Vendor</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="telepon_vendor" placeholder="Telepon Vendor" value="<?= $show_dv["telepon_vendor"]; ?>" required>
                    </div>
                </div>
                
                <!-- HP Vendor -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">HP Vendor</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="HP_vendor" placeholder="HP Vendor" value="<?= $show_dv["hp_vendor"]; ?>">
                    </div>
                </div>
                
                <!-- Contact Person -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Contact Person</label>
                        
                    <div class="col-sm-4">
                       <input type="text" class="form-control" name="contact_person" placeholder="Contact Person" value="<?= $show_dv["cp_vendor"]; ?>" required>
                    </div>
                </div>
                
                <!-- eMail Vendor -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">e-Mail Vendor</label>
                        
                    <div class="col-sm-5">
                        <input type="email" class="form-control" name="email_vendor" placeholder="Email Vendor" value="<?= $show_dv["email_vendor"]; ?>">
                    </div>
                </div>
                
                <div style="margin-left:15px">
                    <h4><u>Informasi Pembayaran</u></h4>
                </div>
                
                <!-- NPWP -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">NPWP</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="npwp" placeholder="NPWP" value="<?= $show_dv["npwp_vendor"]; ?>" required>
                    </div>
                </div>
                
                <!-- BANK -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Bank</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="bank" placeholder="BANK" value="<?= $show_dv["bank_vendor"]; ?>">
                    </div>
                </div>
                
                <!-- No. Rekening -->
                <div class="form-group">    
                    <label class="col-sm-3 control-label">No. Rekening</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="no_rekening" placeholder="No. Rekening" value="<?= $show_dv["no_rekening_vendor"]; ?>">
                    </div>
                </div>
                
                <!-- Catatan -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Catatan</label>
                        
                    <div class="col-sm-4">
                        <textarea class="form-control" name="catatan" placeholder="Catatan"><?= $show_dv["remark_vendor"]; ?></textarea>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>

<script>
    $(document).ready(function(){
        $(".select2").select2();
    });
</script>
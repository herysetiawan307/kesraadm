<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_agent","id_agent,nama_agent,alamat_agent,telepon_agent,hp_agent,cp_agent,email_agent,bank_agent,no_rekening_agent,remark_agent",NULL,"id_agent='$id'");
            $result = $db->getResult();
            foreach($result as $show_da){

?>

            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_update_data_agent.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- ID Agent -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">ID Agent</label>
                        
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="id_agent" placeholder="ID Agent" value="<?= $show_da["id_agent"]; ?>" readonly>
                    </div>
                </div>
                
                <!-- Nama Agent -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama Agent</label>
                        
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="nama_agent" placeholder="Nama Agent" value="<?= $show_da["nama_agent"]; ?>" required>
                    </div>
                </div>
                
                <!-- Alamat Agent -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Alamat Agent</label>
                        
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="alamat_agent" placeholder="Alamat Agent" value="<?= $show_da["alamat_agent"]; ?>" required>
                    </div>
                </div>  
                
                <!-- Telepon Agent -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Telepon Agent</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="telepon_agent" placeholder="Telepon Agent" value="<?= $show_da["telepon_agent"]; ?>" required>
                    </div>
                </div>
                
                <!-- HP Agent -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">HP Agent</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="hp_agent" placeholder="HP Agent" value="<?= $show_da["hp_agent"]; ?>">
                    </div>
                </div>
                
                <!-- Contact Person Agent -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Contact Person Agent</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="cp_agent" placeholder="Contact Person Agent" value="<?= $show_da["cp_agent"]; ?>" required>
                    </div>
                </div>
                
                <!-- eMail Agent -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">e-Mail Agent</label>
                        
                    <div class="col-sm-6">
                        <input type="email" class="form-control" name="email_agent" placeholder="Email Agent" value="<?= $show_da["email_agent"]; ?>">
                    </div>
                </div>
                
                <div style="margin-left:15px">
                    <h4><u>Informasi Pembayaran</u></h4>
                </div>
                
                <!-- Bank -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Bank</label>
                        
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="bank" placeholder="Bank" value="<?= $show_da["bank_agent"]; ?>">
                    </div>
                </div>
                
                <!-- No. Rekening -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">No. Rekening</label>
                        
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="no_rekening" placeholder="No. Rekening" value="<?= $show_da["no_rekening_agent"]; ?>">
                    </div>
                </div>
                
                <!-- Catatan -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Catatan</label>
                        
                    <div class="col-sm-4">
                        <textarea class="form-control" name="catatan" placeholder="Catatan"><?= $show_da["remark_agent"]; ?></textarea>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-4 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>

<!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>

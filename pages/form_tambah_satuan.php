<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
        require_once dirname(__FILE__)."/../components/templates/main.php";
        require_once dirname(__FILE__)."/../class/config.php";

        //Define Connection -> Database
        $db = new Database();
        $db->connect();

        //Call Template
        $template = new Template();

        //Start HTML
        $template->pageTitle="BHUMIKU Balai Pertemuan | New Satuan";

        //Start Content
        $template->contentTitle="<span class='glyphicon glyphicon-edit'></span> New Satuan";
        $template->startContent();
?>

<!-- Box Form -->
<div class="row">
    <div class="col-md-12">
        <?php $template->startBox(); ?>
            <strong></strong>
        <?php $template->conBox();?>
            <!-- Form New Satuan -->
            <form class="form-horizontal" method="POST" action="<?= MAIN_URL ?>/action/act_save_datasatuan.php">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- Kode Satuan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Satuan</label>
                        
                    <div class="col-sm-2">
                       <input type="text" class="form-control" name="kode_satuan" placeholder="Kode Satuan">
                    </div>
                </div>
                
                <!-- Nama Satuan -->
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Satuan</label>
                        
                    <div class="col-sm-3">
                       <input type="text" class="form-control" name="nama_satuan" placeholder="Nama Satuan">
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-2 control-label"></label>
                        
                    <div class="col-sm-1">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
            
        <?php $template->endConBox();?>
        <?php $template->endBox();?>
    </div>
</div>
<!-- End Box Form -->
<!-- End Content -->
<?php $template->endContent(); ?>

    <!-- Place Script Here -->
    
        <!-- Datepicker -->
        <script>
            $(document).ready(function(){
                $("input.datepicker").Zebra_DatePicker();
            });
        </script>
    
        <!-- Select2 -->
        <script>
            $(document).ready(function(){
                $(".select2").select2();
            });
        </script>
        
    <!--// End Script Place -->
    
<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>
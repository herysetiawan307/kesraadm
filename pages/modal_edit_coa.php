<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Required File
    require_once dirname(__FILE__)."/../class/config.php";
    
        
    //Define Connection -> Database
        $db = new Database();
        $db->connect(); 
        
        
        if($_REQUEST["rowid"]){
            $id = $_REQUEST['rowid'];
            $db->select("tb_coa","kode_coa,nama_coa,kode_parent,jenis_coa,saldo_normal",NULL,"kode_coa='$id'");
            $result = $db->getResult();
            foreach($result as $show_dc){
?>
            <form class="form-horizontal" method="POST" action="#">
                <div style="margin-left:15px">
                    <h4><u></u></h4>
                </div>
                <!-- Induk / Parent Akun -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Induk / Parent Akun</label>
                        
                    <div class="col-sm-6">
                        <select class="form-control" name="parent_akun">
                            <?php 
                                $id_c = $show_dc["kode_coa"];
                                $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_coa IN('100000','110000','120000','130000','140000','150000','160000','200000','210000','220000','230000','300000','310000','320000','400000','410000','420000','430000','500000','510000','520000','600000','610000','620000','630000','640000','650000')  AND kode_coa='$id_c' ");
                                $res = $db->getResult();
                                foreach($res as $ult){
                                    print_r($res);
                            ?>
                            <option value="<?= $ult["kode_coa"] ?>"><?= $ult["kode_coa"] ?> - <?= $ult["nama_coa"] ?></option>
                                <?php } ?>
                            
                            <option value=""> ---</option>
                            <?php
                                $db->select("tb_coa","kode_coa,nama_coa",NULL,"kode_coa IN('100000','110000','120000','130000','140000','150000','160000','200000','210000','220000','230000','300000','310000','320000','400000','410000','420000','430000','500000','510000','520000','600000','610000','620000','630000','640000','650000')");
                                $result = $db->getResult();
                                foreach($result as $show_pk){
                            ?>
                            <option value="<?= $show_pk["kode_coa"]; ?>"><?= $show_pk["kode_coa"]; ?> - <?= $show_pk["nama_coa"]; ?></option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
                
                <!-- Kode Akun -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Kode Akun</label>
                        
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="kode_akun" placeholder="Kode Akun" value="<?= $show_dc["kode_coa"]; ?>">
                    </div>
                </div>
                
                <!-- Nama Akun -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nama Akun</label>
                        
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="nama_akun" placeholder="Nama Akun" value="<?= $show_dc["nama_coa"]; ?>">
                    </div>
                </div>
                
                <!-- Jenis Akun -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jenis Akun</label>
                        
                    <div class="col-sm-6">
                        <select class="form-control" name="jenis_akun">
                            <option value="<?= $show_dc["jenis_coa"]; ?>"><?= $show_dc["jenis_coa"]; ?></option>
                            <option value=""> ---</option>
                            <option value="HEADER">Header</option>
                            <option value="DETAIL">Detail</option>
                       </select>
                    </div>
                </div>
                
                <!-- Saldo Normal -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Saldo Normal</label>
                        
                    <div class="col-sm-6">
                        <select class="form-control" name="saldo_normal">
                            <option value="<?= $show_dc["saldo_normal"]; ?>"><?= $show_dc["saldo_normal"]; ?></option>
                            <option value=""> ---</option>
                            <option value="DEBET">Debet</option>
                            <option value="KREDIT">Kredit</option>
                       </select>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="form-group">
                    <label class="col-sm-3 control-label"></label>
                        
                    <div class="col-sm-2">
                        <button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
                    </div>
                    <div class="col-sm-2">
                        <button type="reset" id="reset" name="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
            </form>
        <?php }}?>

<!-- Select2 -->
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });
    </script>